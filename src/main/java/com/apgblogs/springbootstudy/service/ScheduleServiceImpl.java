package com.apgblogs.springbootstudy.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-14 10:19
 */
@Service
public class ScheduleServiceImpl {

    private Logger logger= LoggerFactory.getLogger(ScheduleServiceImpl.class);

    int count1 = 1;
    int count2 = 1;
    int count3 = 1;

    @Scheduled(fixedRate = 1000)
    @Async
    public void jobFixedRate(){
        Thread.currentThread().setName("间隔执行");
        logger.info("{}.执行第{}次",Thread.currentThread().getName(),count1++);
    }

    @Scheduled(fixedRateString = "${job.fixedRate}")
    @Async
    public void jobFixedRateString(){
        Thread.currentThread().setName("spel表达式间隔执行");
        logger.info("{}.执行第{}次",Thread.currentThread().getName(),count2++);
    }

    @Scheduled(fixedDelay = 3000)
    @Async
    public void jobFixedDelay() throws InterruptedException{
        Thread.sleep(1000);
        Thread.currentThread().setName("上次任务完成后间隔执行");
        logger.info("{}.执行第{}次",Thread.currentThread().getName(),count3++);
    }

    @Scheduled(initialDelay = 8000,fixedRate = 1000)
    @Async
    public void jobInitialDelay(){
        Thread.currentThread().setName("spring容器完成后间隔执行");
        logger.info("{}.执行第{}次",Thread.currentThread().getName(),count3++);
    }

    @Scheduled(cron = "0 * 11 * * ?")
    @Async
    public void jobCron(){
        Thread.currentThread().setName("cron表达式执行");
        logger.info("{}.执行第{}次",Thread.currentThread().getName(),count3++);
    }
}
