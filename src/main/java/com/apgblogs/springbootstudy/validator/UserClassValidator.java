package com.apgblogs.springbootstudy.validator;

import com.apgblogs.springbootstudy.vo.UserVo;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-10 17:52
 */
public class UserClassValidator implements ConstraintValidator<UserClassCheck, UserVo> {

    @Override
    public void initialize(UserClassCheck constraintAnnotation) {

    }

    @Override
    public boolean isValid(UserVo userVo, ConstraintValidatorContext constraintValidatorContext) {
        if(userVo == null){
            return true;
        }
        String messageTemplate;
        if(userVo.getUserName().equals("关注我") && userVo.getId().equals("你最帅")){
            return true;
        }
        messageTemplate=String.format("用户名：%1$s和ID：%2$s不匹配",userVo.getUserName(),userVo.getId());
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext.buildConstraintViolationWithTemplate(messageTemplate).addConstraintViolation();
        return false;

    }
}
