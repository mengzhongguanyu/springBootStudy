package com.apgblogs.springbootstudy.repository;

import com.apgblogs.springbootstudy.entity.TUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-13 10:33
 */
public interface UserRepository extends JpaRepository<TUserEntity,String> {
}
